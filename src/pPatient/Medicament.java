package pPatient;

public class Medicament {
	
	private int id;
	private String nom;
	private int quantite;
	private String utilisation;
	
    //Constructeurs
	public Medicament(int id, String nom, int quantite, String utilisation) {
		this.id = id;
		this.nom = nom;
		this.quantite = quantite;
		this.utilisation = utilisation;
	}

	public Medicament(String nom, int quantite, String utilisation) {
		this.nom = nom;
		this.quantite = quantite;
		this.utilisation = utilisation;		
	}
	
	//Accesseurs

	
	//Traitement
}
