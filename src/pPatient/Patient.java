package pPatient;

import pPlanning.*;
import pPersonnel.*;
import pClinique.*;

import java.util.*;

import pBEException.BEException;

public class Patient {
	
	protected int id;
	private String nom, prenom;
	private BEDate dateNaissance;
	private String adresse;
	private String tel, mail;
	private long numSecu;
	private List<RDV> listeRdv;

	
    //Constructeurs
	public Patient(int id, String nom, String prenom, BEDate dateNaissance, String adresse,
			String tel, String mail, long numSecu) {
		
	}

	public Patient(String nom, String prenom, BEDate dateNaissance, String adresse,
			String tel, String mail, long numSecu) {
		
	}
	
	//Accesseurs
	public int getId(){
		return(this.id);
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	//Traitement
	public boolean refresh() throws BEException {
		
		return(false);
	}
	
	public List<RDV> getRdv() throws BEException  {
		
		return(this.listeRdv);
	}
	
	public List<Soins> getFicheSoins(RDV rdv) throws BEException  {
		
		return(null);
	}	

	public boolean addRdv(RDV rdv) throws BEException {
		
		return(false);
	}
	
	
	
}
