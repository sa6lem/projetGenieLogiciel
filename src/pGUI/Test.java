package pGUI;


import pBEException.BEException;
import pClinique.*;

public class Test {

	public static void main(String[] args) {
		
		System.out.println(">START ...");
		
		Clinique cliniqueObj = new Clinique();
		
		try {
			cliniqueObj.init();
			
			System.out.println(cliniqueObj.getNom());
			System.out.println(cliniqueObj.getAdresse());
			
		}catch (BEException e) {
			System.out.println(e.getMsg());
		}
		
		System.out.println(">... END");

	}

}
