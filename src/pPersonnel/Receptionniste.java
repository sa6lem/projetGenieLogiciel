package pPersonnel;

import java.util.*;

import pBDD.*;
import pBEException.*;
import pPatient.*;
import pPlanning.*;

public class Receptionniste extends Personnel {
	
	//Constucteurs
	public Receptionniste(int id, String nom, String prenom, BEDate dateNaissance, 
			String adresse, String tel, String mail, String login, String motPasse,String profession) {
		super(id,nom,prenom,dateNaissance,adresse,tel,mail,login,motPasse,profession);	
	}
	
	public int enregistrerPatient(Patient patient) throws BEException {	
		
		this.patient = patient; 
		
		try {
			this.patient.setId(pBDD.Add.addPatient(patient));
		}catch(BDDException e) {
			throw(new BEException(">error>Receptionniste::init()>acces a la BDD"));
		}
		return(this.patient.getId());
	}
	
	public boolean supprimerPatient() throws BEException {	
		
		return(false);
	}	
	
	public double calculeCoutSoins(RDV rdv) throws BEException {	
		
		return(0);
	}	
}
