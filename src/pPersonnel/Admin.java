package pPersonnel;

import pBEException.*;
import pPlanning.*;
import pClinique.*;
import pPlanning.*;
import pPlanning.*;

public class Admin extends Receptionniste {
	
	public Admin(int id, String nom, String prenom, BEDate dateNaissance, 
			String adresse, String tel, String mail, String login, String motPasse,String profession) {
		super(id,nom,prenom,dateNaissance,adresse,tel,mail,login,motPasse,profession);	
	}
	
    //Constructeurs
	
	
	//Accesseurs

	
	//Traitement
	public boolean ajouterCompteUtilisateur(Personnel personnel) throws BEException {
		return(false);
	}

	public boolean modifierCompteUtilisateur(int id, Personnel personnelnv) throws BEException {
		return(false);
	}

	public boolean supprimerCompteUtilisateur(int id) throws BEException {
		return(false);
	}

	public boolean ajouterServiceClinique(Service service) throws BEException {
		return(false);
	}

	public boolean modifierServiceClinique(int id, Service servicenv) throws BEException {
		return(false);
	}
	
	public boolean supprimerServiceClinique(int id) throws BEException {
		return(false);
	}
	
}
