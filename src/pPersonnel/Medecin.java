package pPersonnel;

import pPlanning.*;
import pClinique.*;
import pPatient.*;

import java.util.*;

import pBEException.BEException;

public class Medecin extends Personnel {
	
	private Planning planning; //identique avec nom service
	private String service;
	
	
	//Constructeurs
	public Medecin(int id, String nom, String prenom, BEDate dateNaissance, 
			String adresse, String tel, String mail, String login, String motPasse,String profession,String service, Planning planning) {
		super(id,nom,prenom,dateNaissance,adresse,tel,mail,login,motPasse,profession);	
		
		this.service = service;
		this.planning = planning;
	}

	public Medecin(String nom, String prenom, BEDate dateNaissance, 
			String adresse, String tel, String mail, String login, String motPasse,String profession,String service, Planning planning) {
		super(nom,prenom,dateNaissance,adresse,tel,mail,login,motPasse,profession);	
		
		this.service = service;
		this.planning = planning;
	}
	
	//Accesseurs
	
	
	//Traitement
	public boolean ajouterFicheSoins(RDV rev, List<Soins> ficheSoins) throws BEException {
		
		return(false);
	}
	
	public boolean ajouterOrdonnance(RDV rev, Ordonnance ordonnance) throws BEException {
		
		return(false);
	}
	
	public Planning consulterPlanningPersonnel() throws BEException {
		
		return(this.planning);
	}	
	

}
