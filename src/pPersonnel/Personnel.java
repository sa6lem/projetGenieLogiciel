package pPersonnel;

import pPlanning.*;

import java.util.List;

import pBDD.BDDException;
import pBEException.BEException;
import pClinique.*;
import pPatient.Patient;
import pPatient.RDV;

public class Personnel {
	
	protected int id;
	protected String nom, prenom;
	protected BEDate dateNaissance;
	protected String adresse;
	protected String tel, mail;
	protected String login, motPasse;
	protected String profession;
	protected Clinique clinique;
	protected Patient patient;
	
	//Constucteurs
	public Personnel (int id, String nom, String prenom, BEDate dateNaissance, 
			String adresse, String tel, String mail, String login, String motPasse,String profession) {
		
	}

	public Personnel (String nom, String prenom, BEDate dateNaissance, 
			String adresse, String tel, String mail, String login, String motPasse,String profession) {
		
	}
	//Accesseurs

	
	//Traitement
	/*
	 * V�rifie si un patient existe dans la BBD
	 * si patient enregistr� return(patient)
	 * si non return(NULL)
	 * Pr�conditions :
	 * 	-numSec � 15 chiffres
	 */
	protected Patient consulterFichePatient(long numSecu) throws BEException {	
		try {
			this.patient = pBDD.Get.getPatient(numSecu);
		}catch(BDDException e) {
			throw(new BEException(">error>Receptionniste::init()>acces a la BDD"));
		}
		return(this.patient);
	}

	protected boolean modifierFichePatient(Patient patientnv) throws BEException {	
		
		return(false);
	}

	protected List<RDV> consulterRdvPatient() throws BEException {	
		
		return(null);
	}

	protected List<RDV> consulterRdvPatient(BEDate date) throws BEException {	
		
		return(null);
	}
	
	protected List<PlanningService> consulterPlanningClinique() throws BEException {	
		
		return(null);
	}	
	
	protected boolean prendreRdvPatient(RDV rdv) throws BEException {	
		
		return(false);
	}
	
	protected boolean modifierRdvPatient(RDV rdv, RDV rdvnv) throws BEException {	
		
		return(false);
	}
	
	protected boolean annulerRdvPatient(RDV rdv) throws BEException {	
		
		return(false);
	}	

	protected List<Personnel> consulterAnnuaireClinique() throws BEException {	
		
		return(null);
	}	
	
	protected boolean configurerComptePerso(Personnel personnel) throws BEException {	
		
		return(false);
	}
}
