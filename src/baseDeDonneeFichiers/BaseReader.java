package baseDeDonneeFichiers;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import user.Person;

public class BaseReader extends BaseSetup {
	
	public Person readMedecin(String id) {
        BufferedReader br = null;
        String line = "";

        try {
            br = new BufferedReader(new FileReader(getUserDataBase()));
            while ((line = br.readLine()) != null) {
            	String[] fields = line.split(separator);
            	if(fields[0].equals(medecin) && fields[1].equalsIgnoreCase(id)) {
            		System.out.println("id = "+ fields[1] + " | Pr�nom : "+ fields[2] +" | Nom : "+ fields[3]);
            	}
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

		return null;
	}
	
}
