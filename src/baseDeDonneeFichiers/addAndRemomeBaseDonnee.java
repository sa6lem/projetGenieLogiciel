package baseDeDonneeFichiers;

import user.Administrateur;
import user.Creneau;
import user.Medecin;
import user.Medicament;
import user.Patient;
import user.Rdv;
import user.Receptionniste;
import user.Salle;
import user.Service;
import user.Soin;

public interface addAndRemomeBaseDonnee {
	
	/**
	 * Description : Ajoute un patient � la base de donn�e et retourne son id unique
	 * @param patient l'objet patient 
	 * @return l'id unique du patient
	 */
	public String addPatient(Patient patient);
	
	/**
	 * Description : Remove un patient � partir de l'identifiant donn�
	 * @param patientID identifiant d'un patient
	 * @return true ou false si le patient � �t� enlever
	 */
	public boolean removePatient(String patientID);
	
	/**
	 * Description :
	 * @param patientId
	 * @return
	 */
	public Rdv addRdv(String patientId);
	
	/**
	 * Description : 
	 * @param rdvId
	 * @return
	 */
	public boolean removeRdvPatient(String rdvId);
	
	public String addService(Service service);
	
	public boolean removeService(String serviceId);
	
	public String addSoin(Soin soin);
	
	public boolean removeSoin(String soinId);
	
	public String addSalle(Salle salle);
	
	public boolean removeSalle(String salleId);
	
	public String addCreneau(Creneau creneau);
	
	public boolean removeCreneau(String creneauId);
	
	public String addMedecin(Medecin medecin);
	
	public boolean removeMedecin(String medecinId);
	
	public String addReceptionniste(Receptionniste receptionniste);
	
	public boolean removeReceptionniste(String receptionnisteId);
	
	public String addAdministrateur(Administrateur administrateur);
	
	public String removeAdministrateur(String administrateurId);

	public String addMedicament(Medicament medicament);
	
	public boolean removeMedicament(String medicamentId);
		
}
