package baseDeDonneeFichiers;

public interface setterBaseDonnee {
	
	public boolean setNomPatient(String idPatient,String nom) ;
	public boolean setPrenomPatient(String idPatient,String prenom) ;
	public boolean setPatientAdresse(String idPatient,String adresse) ;//adresse
	public boolean setPatientTelephone(String idPatient,String telephone) ;//telephone
	public boolean setPatientMail(String idPatient,String mail) ;//mail
	public boolean setPatientCQ(String idPatient,Integer cq) ;//cq
	public boolean setRdvPatient(String rdvId);
	public boolean setReceptionniste();//

}
