package baseDeDonneeFichiers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import user.Person;

public class BaseWritter extends BaseSetup {
	private static Scanner x;
	
	public static void updatePassword(String id,String password) {
		System.out.println("Updating password");
		String filePath = getLoginDataBase();
		File temp = new File("data"+ fileSeparator +"temp.csv");
		File file = new File(filePath);
		String ID;
		String user;
		String pass;
		
		try {
			FileWriter fw = new FileWriter(temp,true);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter pw = new PrintWriter(bw);
			x = new Scanner(new File(filePath));
			x.useDelimiter("[,\n ]");
			
			while(x.hasNext()) {
				ID = x.next();
				user = x.next();
				pass = x.next();
				
				if(ID.equals(id)) {
					pw.println(ID+separator+user+separator+password);
				} else {
					pw.println(ID+separator+user+separator+pass);
				}
			}
			x.close();
			pw.flush();
			pw.close();
			File dump = new File(filePath);
			file.delete();
			temp.renameTo(dump);
		} catch (IOException e) {
			System.out.println("Echec � la modif du fichier");
		} 
	}
	
	public static String addMedecin(Person person) {
		String id = null;
    	try {
    		FileWriter fileWriter = new FileWriter(new File(getUserDataBase()), true);
    		BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
    		id = generateRandomID();
    		
    		//wrinting
			bufferedWriter.write(medecin);
			bufferedWriter.write(separator);
			bufferedWriter.write(id);
			bufferedWriter.write(separator);
			bufferedWriter.write(person.getFirstName());
			bufferedWriter.write(separator);
			bufferedWriter.write(person.getLastName());
			bufferedWriter.write(separator);
			bufferedWriter.write(person.getPhone());
			bufferedWriter.write(separator);
			bufferedWriter.write("\n");
			
			fileWriter.flush();
			bufferedWriter.close();
		} catch (IOException e) {
			System.out.println("Echec � l'�criture du m�decin");
			return null;
		} 
		return id;
	}
	
	public boolean removeMedecin(String id) {
		
		return true;
	}

}
