package baseDeDonneeFichiers;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.UUID;

public class BaseSetup {
	
    protected final static String fileSeparator = System.getProperty("file.separator");
    private final static String baseFolder = "data";
    private final static String userBaseName = "userDataBase";
    private final static String loginBaseName = "loginDataBase";
    private final static String baseExt = ".csv";
    protected final static String separator = ",";
    private final static String basicPassword = "0000";
    protected final static String admin = "admin";
    protected final static String medecin = "medecin";

    
    public static void dataBaseInitialise()  {
    	String uriLoginData = getLoginDataBase();
    	String uriUserData = getUserDataBase();
    	String adminId = iniLoginBaseAdmin(uriLoginData);
    	iniUserBaseAdmin(uriUserData,adminId);
		System.out.println("Done");
		//BaseWritter.updatePassword(adminId, "11111");
    }
    
    public static String getLoginDataBase() {
     	File dataBase = new File(baseFolder + fileSeparator + loginBaseName + baseExt);
		    if(!dataBase.exists()){
		        System.out.println("Initialisation de la login base ");
		        try {
		        	new File(baseFolder).mkdirs();
					dataBase.createNewFile();
					clearFile(dataBase.toString());
				} catch (IOException e) {
					System.out.println("Impossible de cr�er la login dataBase");
				}
		    } else {
		    	System.out.println("Utilisation de la loginDataBase");
		    }
		    return dataBase.toString();
    }
    
    public static String getUserDataBase() {
     	File dataBase = new File(baseFolder + fileSeparator + userBaseName + baseExt);
		    if(!dataBase.exists()){
		        System.out.println("Initialisation de la user base ");
		        try {
		        	//new File(baseFolder).mkdirs();
					dataBase.createNewFile();
					clearFile(dataBase.toString());
				} catch (IOException e) {
					System.out.println("Impossible de cr�er la user dataBase");
				}
		    } else {
		    	System.out.println("Utilisation de la userDataBase");
		    }
		    return dataBase.toString();
    }
    
    public static String generateRandomID() {
		return UUID.randomUUID().toString();
    }
    
    public static void clearFile(String file) throws IOException {
    	FileOutputStream writer = new FileOutputStream(file);
    	writer.write(("").getBytes());
    	writer.close();
    }
    
    private static void iniUserBaseAdmin(String file,String adminId) {
    	FileWriter fileWriter;
		try {
			fileWriter = new FileWriter(new File(file), true);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write(adminId + separator + admin + separator +"\n");
			bufferedWriter.close();
			fileWriter.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    private static String iniLoginBaseAdmin(String file) {
    	FileWriter fileWriter;
    	String id = generateRandomID();
		try {
			fileWriter = new FileWriter(new File(file), true);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write(id+ separator + admin + separator +"0000" + "\n");
			fileWriter.flush();
			bufferedWriter.close();
		} catch (IOException e) {
			System.out.println("Erreur � l'initialisation de la login dataBases");
		}
		return id;
    }
    
}
