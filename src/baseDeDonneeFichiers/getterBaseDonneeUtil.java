package baseDeDonneeFichiers;

import user.Admin;
import user.Creneau;
import user.Medecin;
import user.Medicament;
import user.Ordonnance;
import user.Patient;
import user.Personnel;
import user.Rdv;
import user.Receptionniste;
import user.Salle;
import user.Service;
import user.Soin;

public interface getterBaseDonneeUtil {
	
	/**
	 * Decription : Retourne un objet patient � partir de son num�ro de s�curit� sociale
	 * @param cqNumber num�ro de s�curit� sociale
	 * @return un patient avec toute ses informations
	 */
	public Patient getPatient(Integer cqNumber); 
	
	public <T extends Personnel> T login(String pseudo,String password);
	
	public Service getService(String nomService);
	
	public String getServiceNom(String serviceId);
	
	public Service[] getServices(String clinicId);
	
	public String getCliniqueId();
				
	public Soin[] getSoins(String serviceId);
	
	public String[] getSoinsIds(String serviceId);
	
	public Double getSoinPrix(String soinId);
	
	public String getSoinNom(String soinId);
	
	public String getSoinId(String soinNom);
	
	public Salle[] getSalles(String serviceId);
	
	public Salle[] getSalleIds(String serviceId);
	
	public Salle getSalle(String nomSalle);
	
	public Rdv[] getEmploiDuTempsMedecin(String medecinId);
	
	public Receptionniste getReceptionniste(String receptionnisteId);
	
	public Medecin getMedecin(String medecinId);
	
	public Admin getAdmin(String adminId);
	
	public Rdv[] getRdvPatient(String patientId);
	
	public Ordonnance getOrdonnance(String rdvId);
	
	public Medicament getMedicament(String ordonnanceId);
	
	public Soin getSoin(String rdvId);
	
	
	/**
	 * Description : r�cup�rer tout les cr�neaux d'une salle � partir d'un service donn�
	 * @param serviceId identifiant d'un service
	 * @return tout les objets cr�neaux avec l'horaire et le num�ro de la salle
	 */
	public Creneau[] getCreneauFromService(String serviceId);
	
	/**
	 * Description : r�cup�re tout les cr�neaux libre du m�decin � partir de son Id et de la date du jour
	 * @param medecinId identifiant du m�decin
	 * @param dateJour
	 * @return
	 */
	public Creneau[] getRdvLibreMedecin(String medecinId,String dateJour);
	

}
