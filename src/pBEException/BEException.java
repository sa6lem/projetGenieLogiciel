package pBEException;

public class BEException extends Exception {
	
	private String msg;
	
	public BEException() {
		super();
		this.msg = ">error";
	}

	public BEException(String msg) {
		super();
		this.msg = msg;
	}

	public String getMsg() {
		return(this.msg);
	}
}
