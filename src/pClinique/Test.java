package pClinique;

import pBDD.*;
import pBEException.BEException;

public class Test {

	public static void main(String[] args) {
		System.out.println(">start test");
		
		Clinique clinique = new Clinique();
		try {
			clinique.init();
			
			System.out.println(clinique.getNom()+"\t"+clinique.getAdresse());
			
		}catch (BEException e) {
			
		}
		System.out.println(">end test");
	}

}
