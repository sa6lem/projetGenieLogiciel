package pClinique;

import pPlanning.*;

import java.util.*;

import pBEException.BEException;

public class Salle {
	
	private int id;
	private String nom;
	private boolean enService;
    private Planning planning;
	
    //Constructeurs
	public Salle(int id, String nom, Planning planning) {
		this.id = id;
		this.nom = nom;
		this.enService = false;
		this.planning = planning;
	}

	public Salle(String nom, Planning planning) {
		this.nom = nom;
		this.enService = false;
		this.planning = planning;
	}
	
	//Accesseurs
	public Planning getPlanning() throws BEException {
		
		return(this.planning);
	}
	
	//Traitement
	public boolean refresh() throws BEException {
		
		return(false);
	}
	
}
