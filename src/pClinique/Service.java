package pClinique;

import pPlanning.*;
import pPersonnel.*;

import java.util.*;

import pBEException.BEException;

public class Service {
	
	private int id;
	private String nom;
	private int TCrn;
	private List<Salle> listeSalles;
	private List<Soins> listeSoins;
	private PlanningService planning;
	private List<Medecin> listeMedecins;
	
	//Constucteurs
	public Service(int id, String nom, int TCrn, PlanningService Planning) {
		this.id = id;
		this.nom = nom;
		this.TCrn = TCrn;
		this.planning = planning;
		
		this.listeSalles = new ArrayList<Salle>();
		this.listeSoins = new ArrayList<Soins>();
		this.listeMedecins = new ArrayList<Medecin>();
	}

	public Service(String nom, int TCrn, PlanningService Planning) {
		this.nom = nom;
		this.TCrn = TCrn;
		this.planning = planning;
		
		this.listeSalles = new ArrayList<Salle>();
		this.listeSoins = new ArrayList<Soins>();
		this.listeMedecins = new ArrayList<Medecin>();
	}
	
	//Accesseurs
	public PlanningService getPlanning() throws BEException {
		
		return(this.planning);
	}
	
	//Traitement
	public boolean refresh() throws BEException {
		return(false);
	}
	
	public boolean addSoins(Soins soin) throws BEException {
		
		return(false);
	}

}
