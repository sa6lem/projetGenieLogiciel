package pClinique;

import pBDD.*;
import pBEException.*;
import pPersonnel.*;
import pPlanning.*;

import java.util.*;
 

public class Clinique {
	
	private String nom;
	private String adresse;
	
	private List<Service> listeServices;
	private List<Personnel> listePersonnels;
	
	
	//constructeurs
	public Clinique() {
		this.listeServices = new ArrayList<Service>();
		this.listePersonnels = new ArrayList<Personnel>();
	}
	
	//accesseurs
	public String getNom() {
		return(this.nom);
	}
	public String getAdresse() {
		return(this.adresse);
	}
	
	//traitements
	public boolean init() throws BEException {
		try {
			this.nom = pBDD.Get.getNomClinique();
			this.adresse = pBDD.Get.getAdresseClinique();
			
		}catch(BDDException e) {
			throw(new BEException(">error>Clinique::init()>acces a la BDD"));
		}
		
		return(true);
	}
	
	private boolean refresh() throws BEException {
		return(this.init());
	}
	
	/*
	 * v�rifie si le personnel id existe dans la liste du personnel
	 * si existe: renvoie l'objet Personnel correspondant � l'id
	 * sinon renvoie null
	 */
	private Personnel checkToPersonnel(int id) throws BEException {
		
		return(null);
	}
	
	private PlanningService getPlanningService(int id) throws BEException {
		
		return(null);
	}
	
	private Planning getPlanning() throws BEException {
		
		return(null);
	}

	
}
