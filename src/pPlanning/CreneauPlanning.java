package pPlanning;

public class CreneauPlanning extends Creneau {
	
	enum CRTYPE{RDV,EVENT};
	
	private int id;
	private CRTYPE type;
	
	public CreneauPlanning(String jour, BEDate date, BETime timeDeb, BETime timeFin, String commentaire, CRTYPE type) {
		super(jour, date, timeDeb, timeFin, commentaire);
		
		this.type = type;
	}

}
