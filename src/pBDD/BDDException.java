package pBDD;

public class BDDException extends Exception {
	
	private String msg;
	
	public BDDException() {
		super();
		this.msg = ">error";
	}

	public BDDException(String msg) {
		super();
		this.msg = msg;
	}
	
	public String getMsg() {
		return(this.msg);
	}
}


