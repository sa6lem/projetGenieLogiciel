package user;

public class Person {
    //All final attributes
    private final String firstName; // required
    private final String lastName; // required
    private String id;  //required
    private String phone; // optional
    private String address; // optional
    private String fonction;
 
    private Person(builder builder) {
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.phone = builder.phone;
        this.address = builder.address;
        this.id = builder.id;
        this.fonction = builder.fonction;
    }
    
    public String getFirstName() {
        return firstName;
    }
    
    public String getLastName() {
        return lastName;
    }
    
    public String getPhone() {
        return phone;
    }
    public boolean setPhone(String phone) {
    	this.phone = phone;
    	return true;
    }
    public String getAddress() {
        return address;
    }
    public boolean setAddress(String address) {
    	this.address = address;
    	return true;
    }
    
    public String getId() {
        return id;
    }

	
	public String getFonction() {
		return fonction;
	}

 
    
    public String toString() {
        return "Person: "+this.firstName+", "+this.lastName+", "+this.fonction+", "+this.phone+", "+this.address;
    }
 
    public static class builder 
    {
        private final String firstName;
        private final String lastName;
        private String id;
        private String phone;
        private String address;
        private String fonction;
 
        public builder(String firstName, String lastName,String id) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.id = id;
            this.phone = "";
            this.address = "";
            this.fonction = "";
            
        }
        public builder phone(String phone) {
            this.phone = phone;
            return this;
        }
        public builder id(String id) {
            this.id = id;
            return this;
        }
        public builder address(String address) {
            this.address = address;
            return this;
        }
        public Person build() {
            return  new Person(this);
        }
        public builder fonction(String fonction) {
        	this.fonction = fonction;
        	return this;
        }
    }
}